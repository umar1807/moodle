FROM ubuntu:latest 
 
RUN apt update -y && \ 
    ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime 
 
RUN apt install apache2 -y 

RUN apt install software-properties-common -y

RUN add-apt-repository ppa:ondrej/php -y

RUN apt update -y
 
RUN apt install php7.0 php7.0-cli php7.0-common php7.0-json php7.0-opcache php7.0-mysql php7.0-mbstring php7.0-mcrypt php7.0-zip php7.0-fpm php7.0-curl php7.0-xml php7.0-soap php7.0-xmlrpc php7.0-gd php7.0-intl -y

RUN apt install libapache2-mod-php7.0 -y

RUN mkdir /var/www/html/moodle

ADD ./moodle/ /var/www/html/moodle/

RUN mkdir /var/www/html/moodledata

RUN chown -R www-data:www-data /var/www/html/moodle/ && \ 
    chmod -R 755 /var/www/html/moodle/ && \ 
    chown www-data /var/www/html/moodledata 

COPY moodle.conf /etc/apache2/sites-available/ 

RUN a2enmod rewrite && \ 
    a2ensite moodle.conf && \ 
    a2dissite 000-default.conf 

RUN service apache2 restart 
 
EXPOSE 80 
 
CMD ["apachectl","-D","FOREGROUND"]
